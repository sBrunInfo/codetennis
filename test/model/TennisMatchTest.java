package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TennisMatchTest {

    private TennisMatch tennisMatch;

    @BeforeEach
    void setUp() {
        tennisMatch = new TennisMatch();
    }

    @Test
    void init() {
        final String scoreString = tennisMatch.init();

        Assertions.assertEquals(tennisMatch.getScoreString(), scoreString);

        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
        for (int i = 0; i < TennisConstants.NB_SETS_TOTAL; i++) {
            Assertions.assertEquals(0, tennisMatch.getPlayerOne().getSets().get(i));
            Assertions.assertEquals(0, tennisMatch.getPlayerTwo().getSets().get(i));
        }
    }

    @Test
    void setScore() {
        final List<Integer> p1sets = new ArrayList<>(Arrays.asList(1, 6, 3));
        final TennisScore p1score = TennisScore.FIFTEEN;
        final List<Integer> p2sets = new ArrayList<>(Arrays.asList(6, 7, 5));
        final TennisScore p2score = TennisScore.THIRTY;

        tennisMatch.setScore(p1sets, p1score, p2sets, p2score);

        Assertions.assertEquals(p1sets, tennisMatch.getPlayerOne().getSets());
        Assertions.assertEquals(p1score, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(p2sets, tennisMatch.getPlayerTwo().getSets());
        Assertions.assertEquals(p2score, tennisMatch.getPlayerTwo().getCurrentGameScore());
        Assertions.assertEquals(2, tennisMatch.getCurrentSet());
    }

    @Test
    void getCurrentSet() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(1, 7, 6)));
        tennisMatch.getPlayerTwo().setSets(new ArrayList<>(Arrays.asList(6, 6, 6)));

        Assertions.assertEquals(2, tennisMatch.getCurrentSet());
    }

    @Test
    void getPlayerWonSets() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(1, 7, 6)));
        tennisMatch.getPlayerTwo().setSets(new ArrayList<>(Arrays.asList(6, 6, 6)));

        Assertions.assertEquals(1, tennisMatch.getPlayerOneWonSet());
        Assertions.assertEquals(1, tennisMatch.getPlayerTwoWonSet());
    }

    @Test
    void playerScores_returnScores() {
        final String result = tennisMatch.playerScores(tennisMatch.getPlayerOne());
        Assertions.assertEquals(tennisMatch.getScoreString(), result);
    }

    @Test
    void playerScores_doNothingForOtherPlayer() {
        final List<Integer> p2sets = tennisMatch.getPlayerTwo().getSets();
        final TennisScore p2score = tennisMatch.getPlayerTwo().getCurrentGameScore();

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(p2sets, tennisMatch.getPlayerTwo().getSets());
        Assertions.assertEquals(p2score, tennisMatch.getPlayerTwo().getCurrentGameScore());
    }

    @Test
    void playerScores_love_to_fifteen() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.LOVE);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.FIFTEEN, tennisMatch.getPlayerOne().getCurrentGameScore());
    }

    @Test
    void playerScores_fifteen_to_thirty() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FIFTEEN);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.THIRTY, tennisMatch.getPlayerOne().getCurrentGameScore());
    }

    @Test
    void playerScores_thirty_to_fourty() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.THIRTY);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.FOURTY, tennisMatch.getPlayerOne().getCurrentGameScore());
    }

    @Test
    void playerScores_deuce_to_av() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        tennisMatch.getPlayerTwo().setCurrentGameScore(TennisScore.FOURTY);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.ADVANTAGE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.FOURTY, tennisMatch.getPlayerTwo().getCurrentGameScore());
    }

    @Test
    void playerScores_av_to_deuce() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        tennisMatch.getPlayerTwo().setCurrentGameScore(TennisScore.ADVANTAGE);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.FOURTY, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.FOURTY, tennisMatch.getPlayerTwo().getCurrentGameScore());
    }

    @Test
    void playerScores_fourty_to_game() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        final int formetSetsWon = tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet());

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
        Assertions.assertEquals(formetSetsWon + 1, tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet()));
    }

    @Test
    void playerScores_av_to_game() {
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.ADVANTAGE);
        final int formerGamesWon = tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet());

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
        Assertions.assertEquals(formerGamesWon + 1, tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet()));
    }

    @Test
    void playerScores_set_won() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(5, 0, 0)));
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        final int formerSetNumber = tennisMatch.getCurrentSet();
        final int formerGamesWon = tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet());

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
        Assertions.assertEquals(formerGamesWon + 1, tennisMatch.getPlayerOne().getSets().get(formerSetNumber));
        Assertions.assertEquals(formerSetNumber + 1, tennisMatch.getCurrentSet());
    }

    @Test
    void playerScores_tiebreak() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(5, 0, 0)));
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        tennisMatch.getPlayerTwo().setSets(new ArrayList<>(Arrays.asList(5, 0, 0)));
        tennisMatch.getPlayerTwo().setCurrentGameScore(TennisScore.THIRTY);
        final int formerSetNumber = tennisMatch.getCurrentSet();
        final int formerGamesWonP1 = tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet());
        final int formerGamesWonP2 = tennisMatch.getPlayerTwo().getSets().get(tennisMatch.getCurrentSet());

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
        Assertions.assertEquals(formerGamesWonP1 + 1, tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet()));
        Assertions.assertEquals(formerGamesWonP2, tennisMatch.getPlayerTwo().getSets().get(tennisMatch.getCurrentSet()));
        Assertions.assertEquals(formerSetNumber, tennisMatch.getCurrentSet());
    }

    @Test
    void playerScores_tiebreak_won() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(6, 0, 0)));
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        tennisMatch.getPlayerTwo().setSets(new ArrayList<>(Arrays.asList(6, 0, 0)));
        tennisMatch.getPlayerTwo().setCurrentGameScore(TennisScore.THIRTY);
        final int formerSetNumber = tennisMatch.getCurrentSet();
        final int formerGamesWonP1 = tennisMatch.getPlayerOne().getSets().get(tennisMatch.getCurrentSet());
        final int formerGamesWonP2 = tennisMatch.getPlayerTwo().getSets().get(tennisMatch.getCurrentSet());

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
        Assertions.assertEquals(formerGamesWonP1 + 1, tennisMatch.getPlayerOne().getSets().get(formerSetNumber));
        Assertions.assertEquals(formerGamesWonP2, tennisMatch.getPlayerTwo().getSets().get(formerSetNumber));
        Assertions.assertEquals(formerSetNumber + 1, tennisMatch.getCurrentSet());
    }

    @Test
    void playerScores_reset_match_won_2_to_1() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(6, 7, 6)));
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        tennisMatch.getPlayerTwo().setSets(new ArrayList<>(Arrays.asList(4, 6, 5)));
        tennisMatch.getPlayerTwo().setCurrentGameScore(TennisScore.THIRTY);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(new ArrayList<>(Arrays.asList(0, 0, 0)), tennisMatch.getPlayerOne().getSets());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(new ArrayList<>(Arrays.asList(0, 0, 0)), tennisMatch.getPlayerTwo().getSets());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
    }

    @Test
    void playerScores_reset_match_won_2_to_0() {
        tennisMatch.getPlayerOne().setSets(new ArrayList<>(Arrays.asList(6, 5, 0)));
        tennisMatch.getPlayerOne().setCurrentGameScore(TennisScore.FOURTY);
        tennisMatch.getPlayerTwo().setSets(new ArrayList<>(Arrays.asList(4, 0, 0)));
        tennisMatch.getPlayerTwo().setCurrentGameScore(TennisScore.THIRTY);

        tennisMatch.playerScores(tennisMatch.getPlayerOne());

        Assertions.assertEquals(new ArrayList<>(Arrays.asList(0, 0, 0)), tennisMatch.getPlayerOne().getSets());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerOne().getCurrentGameScore());
        Assertions.assertEquals(new ArrayList<>(Arrays.asList(0, 0, 0)), tennisMatch.getPlayerTwo().getSets());
        Assertions.assertEquals(TennisScore.LOVE, tennisMatch.getPlayerTwo().getCurrentGameScore());
    }

    @Test
    void getScoreString() {
        Assertions.assertEquals(
                "Player 1 : " + tennisMatch.getPlayerOne().getScoreString() + '\n' +
                        "Player 2 : " + tennisMatch.getPlayerTwo().getScoreString(),
                tennisMatch.getScoreString()
        );
    }
}