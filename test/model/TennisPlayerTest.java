package model;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TennisPlayerTest {

    private TennisPlayer tennisPlayer;

    @BeforeEach
    void setUp() {
        tennisPlayer = new TennisPlayer();
    }

    @Test
    void getScoreString_1_0_0_0() {
        tennisPlayer.setSets(new ArrayList<>(Arrays.asList(1, 0, 0)));
        tennisPlayer.setCurrentGameScore(TennisScore.LOVE);
        Assertions.assertEquals("1 | 0 | 0 --- 0", tennisPlayer.getScoreString());
    }

    @Test
    void getScoreString_1_2_0_15() {
        tennisPlayer.setSets(new ArrayList<>(Arrays.asList(1, 2, 0)));
        tennisPlayer.setCurrentGameScore(TennisScore.FIFTEEN);
        Assertions.assertEquals("1 | 2 | 0 --- 15", tennisPlayer.getScoreString());
    }

    @Test
    void getScoreString_1_2_3_30() {
        tennisPlayer.setSets(new ArrayList<>(Arrays.asList(1, 2, 3)));
        tennisPlayer.setCurrentGameScore(TennisScore.THIRTY);
        Assertions.assertEquals("1 | 2 | 3 --- 30", tennisPlayer.getScoreString());
    }

    @Test
    void getScoreString_0_4_0_40() {
        tennisPlayer.setSets(new ArrayList<>(Arrays.asList(0, 4, 0)));
        tennisPlayer.setCurrentGameScore(TennisScore.FOURTY);
        Assertions.assertEquals("0 | 4 | 0 --- 40", tennisPlayer.getScoreString());
    }

    @Test
    void getScoreString_0_0_0_Av() {
        tennisPlayer.setSets(new ArrayList<>(Arrays.asList(0, 0, 0)));
        tennisPlayer.setCurrentGameScore(TennisScore.ADVANTAGE);
        Assertions.assertEquals("0 | 0 | 0 --- Av.", tennisPlayer.getScoreString());
    }
}
