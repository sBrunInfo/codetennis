package model;

import java.util.ArrayList;
import java.util.List;

public class TennisPlayer {

    // Each integer = one set representing the number of games won
    private List<Integer> sets;
    private TennisScore currentGameScore;

    public TennisPlayer() {
        sets = new ArrayList<>();
        for (int i = 0; i < TennisConstants.NB_SETS_TOTAL; i++) {
            sets.add(0);
        }
        currentGameScore = TennisScore.LOVE;
    }

    public List<Integer> getSets() {
        return sets;
    }

    public void setSets(List<Integer> sets) {
        if (sets.size() != TennisConstants.NB_SETS_TOTAL) {
            throw new IllegalArgumentException("The size of the parameter list (number of sets) should always be " + TennisConstants.NB_SETS_TOTAL);
        }
        for (final Integer setScore : sets) {
            if (setScore == null) {
                throw new IllegalArgumentException("Cannot put null as the number of games won");
            }
            if (setScore < 0) {
                throw new IllegalArgumentException("Cannot put a negative number of games won");
            }
        }
        this.sets = sets;
    }

    public TennisScore getCurrentGameScore() {
        return currentGameScore;
    }

    public void setCurrentGameScore(TennisScore currentGameScore) {
        if (currentGameScore == null) {
            throw new IllegalArgumentException("Cannot put null as the current game score");
        }
        this.currentGameScore = currentGameScore;
    }

    public String getScoreString() {
        return sets.get(0) + " | " + sets.get(1) + " | " + sets.get(2) + " --- " + currentGameScore;
    }
}
