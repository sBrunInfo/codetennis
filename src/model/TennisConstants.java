package model;

public class TennisConstants {

    public static final int NB_SETS_TOTAL = 3;
    public static final int NB_SETS_TO_WIN = (NB_SETS_TOTAL / 2) + 1;
    public static final int NB_GAMES_TO_WIN_SET = 6;
    public static final int NB_GAMES_TO_FORCE_TIEBREAK = NB_GAMES_TO_WIN_SET - 1;
    public static final int NB_GAMES_TO_WIN_TIEBREAK = NB_GAMES_TO_WIN_SET + 1;

    // Not supposed to be instanciated
    private TennisConstants() {
    }
}
