package model;

import java.util.ArrayList;
import java.util.List;

public class TennisMatch {

    private final TennisPlayer playerOne;
    private final TennisPlayer playerTwo;

    public TennisMatch() {
        playerOne = new TennisPlayer();
        playerTwo = new TennisPlayer();
    }

    public TennisPlayer getPlayerOne() {
        return playerOne;
    }

    public TennisPlayer getPlayerTwo() {
        return playerTwo;
    }

    public String init() {
        final List<Integer> playerOneSets = new ArrayList<>();
        final List<Integer> playerTwoSets = new ArrayList<>();
        for (int i = 0; i < TennisConstants.NB_SETS_TOTAL; i++) {
            playerOneSets.add(0);
            playerTwoSets.add(0);
        }

        playerOne.setSets(playerOneSets);
        playerOne.setCurrentGameScore(TennisScore.LOVE);
        playerTwo.setSets(playerTwoSets);
        playerTwo.setCurrentGameScore(TennisScore.LOVE);

        return getScoreString();
    }

    public void setScore(List<Integer> playerOneSets, TennisScore playerOneScore, List<Integer> playerTwoSets, TennisScore playerTwoScore) {
        playerOne.setSets(playerOneSets);
        playerOne.setCurrentGameScore(playerOneScore);
        playerTwo.setSets(playerTwoSets);
        playerTwo.setCurrentGameScore(playerTwoScore);
    }

    public int getCurrentSet() {
        int result = 0;
        for (int i = 0; i < TennisConstants.NB_SETS_TOTAL; i++) {
            final int playerOneWonGames = playerOne.getSets().get(i);
            final int playerTwoWonGames = playerTwo.getSets().get(i);

            if (
                    (playerOneWonGames >= TennisConstants.NB_GAMES_TO_WIN_SET && playerTwoWonGames < TennisConstants.NB_GAMES_TO_FORCE_TIEBREAK) ||
                            (playerOneWonGames < TennisConstants.NB_GAMES_TO_FORCE_TIEBREAK && playerTwoWonGames >= TennisConstants.NB_GAMES_TO_WIN_SET) ||
                            playerOneWonGames >= TennisConstants.NB_GAMES_TO_WIN_TIEBREAK ||
                            playerTwoWonGames >= TennisConstants.NB_GAMES_TO_WIN_TIEBREAK
            ) {
                result++;
            } else {
                break;
            }
        }
        return result;
    }

    public int getPlayerOneWonSet() {
        return getPlayerWonSets(playerOne);
    }

    public int getPlayerTwoWonSet() {
        return getPlayerWonSets(playerTwo);
    }

    protected int getPlayerWonSets(TennisPlayer targetPlayer) {
        TennisPlayer otherPlayer;
        if (targetPlayer == playerOne) {
            otherPlayer = playerTwo;
        } else if (targetPlayer == playerTwo) {
            otherPlayer = playerOne;
        } else {
            throw new IllegalArgumentException("You may call getPlayerWonSets() only with a player already in this TennisMatch");
        }

        int result = 0;
        final int currentSet = getCurrentSet();
        for (int i = 0; i < currentSet; i++) {
            if (targetPlayer.getSets().get(i) > otherPlayer.getSets().get(i)) {
                result++;
            }
        }
        return result;
    }

    public String playerOneScores() {
        return playerScores(playerOne);
    }

    public String playerTwoScores() {
        return playerScores(playerTwo);
    }

    protected String playerScores(TennisPlayer scoringPlayer) {
        TennisPlayer otherPlayer;
        if (scoringPlayer == playerOne) {
            otherPlayer = playerTwo;
        } else if (scoringPlayer == playerTwo) {
            otherPlayer = playerOne;
        } else {
            throw new IllegalArgumentException("You may call playerScores() only with a player already in this TennisMatch");
        }

        if (scoringPlayer.getCurrentGameScore().equals(TennisScore.LOVE)) {
            scoringPlayer.setCurrentGameScore(TennisScore.FIFTEEN);
        } else if (scoringPlayer.getCurrentGameScore().equals(TennisScore.FIFTEEN)) {
            scoringPlayer.setCurrentGameScore(TennisScore.THIRTY);
        } else if (scoringPlayer.getCurrentGameScore().equals(TennisScore.THIRTY)) {
            scoringPlayer.setCurrentGameScore(TennisScore.FOURTY);
        } else if (scoringPlayer.getCurrentGameScore().equals(TennisScore.FOURTY)) {
            if (otherPlayer.getCurrentGameScore().equals(TennisScore.ADVANTAGE)) {
                otherPlayer.setCurrentGameScore(TennisScore.FOURTY);
            } else if (otherPlayer.getCurrentGameScore().equals(TennisScore.FOURTY)) {
                scoringPlayer.setCurrentGameScore(TennisScore.ADVANTAGE);
            } else { // lower scores
                playerWinsGame(scoringPlayer, otherPlayer);
            }
        } else { // scoringPlayer.getCurrentGameScore().equals(TennisScore.ADVANTAGE)
            playerWinsGame(scoringPlayer, otherPlayer);
        }

        final String result = getScoreString();

        // Checking match win conditions
        if (getPlayerOneWonSet() >= TennisConstants.NB_SETS_TO_WIN) {
            System.out.println("PLAYER 1 WON THE MATCH !");
            init();
        } else if (getPlayerTwoWonSet() >= TennisConstants.NB_SETS_TO_WIN) {
            System.out.println("PLAYER 2 WON THE MATCH !");
            init();
        }

        return result;
    }

    private void playerWinsGame(TennisPlayer scoringPlayer, TennisPlayer otherPlayer) {
        scoringPlayer.setCurrentGameScore(TennisScore.LOVE);
        otherPlayer.setCurrentGameScore(TennisScore.LOVE);

        final int currentSet = getCurrentSet();
        final int gamesWon = scoringPlayer.getSets().get(currentSet) + 1;
        scoringPlayer.getSets().set(currentSet, gamesWon);
    }

    public String getScoreString() {
        return "Player 1 : " + playerOne.getScoreString() + '\n' +
                "Player 2 : " + playerTwo.getScoreString();
    }
}
