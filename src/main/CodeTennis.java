package main;

import java.util.Scanner;
import model.TennisMatch;

public class CodeTennis {

    public static void main(String[] args) {
        final TennisMatch tennisMatch = new TennisMatch();
        String scoreString = tennisMatch.init();

        final Scanner scanner = new Scanner(System.in);
        String input;

        System.out.println("WELCOME TO CODE TENNIS");
        System.out.println("----------------------");

        do {
            System.out.println("Current scores :\n" + scoreString + '\n');
            System.out.println("Type 1 or 2 to make a player score, or q to quit");
            input = scanner.nextLine();

            switch (input) {
                case "1":
                    System.out.println("Player 1 scores !");
                    scoreString = tennisMatch.playerOneScores();
                    break;
                case "2":
                    System.out.println("Player 2 scores !");
                    scoreString = tennisMatch.playerTwoScores();
                    break;
                case "q":
                    break;
                default:
                    System.out.println("Unrecognized input...");
            }
        } while (!input.equals("q"));
        System.out.println("Bye !");
    }
}
