# Techno utilisée
**Java**, pour deux raisons :
1. c'est le language avec lequel je me sens le plus "à l'aise"
2. *(le plus important)* grâce à IntelliJ, la génération d'exécutable *(.jar)* et JUnit fonctionnent pratiquement "out of the box", pour perdre le moins de temps possible

# Temps passé
**Trois heures**, ce qui semble imposant, mais qui est justifié par :
1. le sujet qui possède des subtilités qui sont facilement ratées à première vue *(par exemple, gestion des scores 40-40 et des sets à 6-5)*
2. le fait que j'aie pris le temps de faire l'exercice en ***Test Driven Development***, ce qui m'a forcé à *blinder* mon code de tests (probablement, pour le meilleur, au final !)

# Comment lancer
L'application en **ligne de commande** est entièrement fonctionnelle et ne requiert que **Java 8** d'installé.

Pour l'exécuter :

`java -jar (project root)/artifact/CodeTennis.jar`